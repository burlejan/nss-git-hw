# Nazev Aplikace

## List Autorů
- [Jan Burle](mailto:burlejan@fel.cvut.cz)

## List Technologií
- Spring Boot
- React
- REST api
- PostgreSQL
- Spring Cloud API GW
- Kubernetes
- Chat GPT

## Popis
Toto je velice komplexní aplikace. V této sekci stručně popíšeme, co aplikace dělá, proč by měla být používána a jaké jsou její klíčové vlastnosti.

Pět set servis má aplikace na nic
spoustu technologií, nedělá nic. 

## Způsob Instalace
* rm -rf

## Copyright
© [Jan Burle](mailto:burlejan@fel.cvut.cz). Všechna práva vyhrazena.